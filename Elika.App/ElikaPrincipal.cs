﻿using System;
using System.Security.Principal;

namespace Elika.App
{
    /// <summary>
    ///     Основная информация
    /// </summary>
    public class ElikaPrincipal : IPrincipal
    {
        private readonly UserScope _userScope;

        internal ElikaPrincipal(UserScope userScope)
        {
            _userScope = userScope;
            Identity = new ElikaIdentity(userScope);
        }

        internal ElikaPrincipal()
        {
            Identity = new ElikaIdentity();
        }

        /// <inheritdoc />
        public bool IsInRole(string role)
        {
            if (role == null)
                throw new ArgumentNullException(nameof(role));

            if (role == string.Empty)
                throw new ArgumentException();

            var normalizeRole = role.ToLower();

            return _userScope.GetUserRoles()?.Contains(normalizeRole) ?? false;
        }

        /// <inheritdoc />
        public IIdentity Identity { get; }

        internal static ElikaPrincipal Empty { get; } = new ElikaPrincipal();
    }
}
