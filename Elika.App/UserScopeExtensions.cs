﻿using System.Collections.Generic;
using Elika.App.Auth;

namespace Elika.App
{
    internal static class UserScopeExtensions
    {
        public static SessionInfo GetSessionInfo(this UserScope userScope)
        {
            return userScope.GetData<SessionInfo>(UserScopeKeys.UserSession);
        }

        public static void SetSessionInfo(this UserScope userScope, SessionInfo sessionInfo)
        {
            userScope.SetData(UserScopeKeys.UserSession, sessionInfo);
        }

        public static UserInfo GetUserInfo(this UserScope userScope)
        {
            return userScope.GetData<UserInfo>(UserScopeKeys.UserInfo);
        }

        public static void SetUserInfo(this UserScope userScope, UserInfo userInfo)
        {
            userScope.SetData(UserScopeKeys.UserInfo, userInfo);
        }

        public static HashSet<string> GetUserRoles(this UserScope userScope)
        {
            return userScope.GetData<HashSet<string>>(UserScopeKeys.UserRoles);
        }
    }
}
