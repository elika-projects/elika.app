﻿using System.Security.Principal;
using Elika.App.Auth;

namespace Elika.App
{
    /// <summary>
    ///     Личность пользователя
    /// </summary>
    public class ElikaIdentity : IIdentity
    {
        private readonly UserScope _userScope;

        /// <summary>
        ///     Инициализировать тип <see cref="ElikaApplication"/> с указанием пространсва пользователя
        /// </summary>
        /// <param name="userScope">Пространство пользователя</param>
        public ElikaIdentity(UserScope userScope)
        {
            _userScope = userScope;
        }

        internal ElikaIdentity() { }

        /// <inheritdoc cref="SessionInfo.AccessToken"/>
        public string AccessToken => _userScope.GetSessionInfo()?.AccessToken;

        /// <inheritdoc />
        public string AuthenticationType => _userScope.GetSessionInfo()?.AuthenticationType;

        /// <inheritdoc />
        public bool IsAuthenticated => _userScope.GetSessionInfo() != null;

        /// <inheritdoc />
        public string Name => _userScope.GetUserInfo()?.UserName;
    }
}
