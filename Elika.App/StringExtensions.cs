﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elika.App
{
    internal static class StringExtensions
    {
        public static string[] ParseRoles(this IEnumerable<string> str, char splitter)
        {
            var splitterRoles = str.Select(role => role.ToLower()).SelectMany(role =>
                {
                    var chunkCount = role.Count(c => c == splitter);
                    var roles = new string[chunkCount + 1];
                    roles[0] = role;

                    var prevPos = role.Length - 1;
                    var lastSubRole = role;
                    for (int i = 0; i < chunkCount; i++)
                    {
                        prevPos = role.LastIndexOf(splitter, lastSubRole.Length - 1);
                        lastSubRole = role.Substring(0, prevPos);
                        roles[i + 1] = lastSubRole;
                    }

                    return roles;
                })
                .Distinct()
                .ToArray();

            return splitterRoles;
        }
    }
}
