﻿using System;
using System.Security.Principal;
using Elika.App.Auth;
using Elika.App.Managers;
using Elika.Collection;
using Microsoft.Extensions.DependencyInjection;

namespace Elika.App
{
    /// <summary>
    ///     Пространство пользователя
    /// </summary>
    public class UserScope : DataCollection, IServiceProvider
    {
        internal UserScope(IServiceProvider rootServiceProvider)
        {
            RootServiceProvider = rootServiceProvider;
            CurrentServiceScope = rootServiceProvider.CreateScope();
        }

        /// <summary>
        ///     Провайдер пространства пользователя
        /// </summary>
        public IServiceProvider RootServiceProvider { get; }

        /// <summary>
        ///    Текущее пространство сервисов
        /// </summary>
        public IServiceScope CurrentServiceScope { get; }

        /// <summary>
        ///     Текущий пользователь
        /// </summary>
        public IPrincipal Principal => RootServiceProvider.GetRequiredService<IPrincipalStore>().GetPrincipal();

        /// <summary>
        ///     Признак аутентификации пользователя
        /// </summary>
        public bool IsAuthenticated => Principal?.Identity?.IsAuthenticated ?? false;

        /// <summary>
        ///     Текущее приложение
        /// </summary>
        public ElikaApplication Application => RootServiceProvider.GetRequiredService<ElikaApplication>();

        /// <summary>
        ///     Авторизовать пользователя
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        public void Authorize(string userName, string password)
        {
            var authService = RootServiceProvider.GetRequiredService<UserManager>();

            if (Principal.Identity.IsAuthenticated)
                authService.Logout();

            authService.Authorize(userName, password);

            OnAuthorized?.Invoke(this, userName);
        }

        /// <summary>
        ///     Выход пользователя
        /// </summary>
        public void Logout()
        {
            var authService = RootServiceProvider.GetRequiredService<UserManager>();
            authService.Logout();
            OnLogout?.Invoke(this);
        }

        /// <summary>
        ///     Делегат авторизации пользователя
        /// </summary>
        /// <param name="scope">Пространство пользователя</param>
        /// <param name="userName">Имя пользователя</param>
        public delegate void DelOnAuthorized(UserScope scope, string userName);

        /// <summary>
        ///     Событие авторизации пользователя
        /// </summary>
        public event DelOnAuthorized OnAuthorized;

        /// <summary>
        ///     Делегат выхода пользователя
        /// </summary>
        /// <param name="scope">Пространство пользователя</param>
        public delegate void DelOnLogout(UserScope scope);

        /// <summary>
        ///     Событие выхода пользователя
        /// </summary>
        public event DelOnLogout OnLogout;

        /// <inheritdoc />
        public object GetService(Type serviceType)
        {
            return CurrentServiceScope.ServiceProvider.GetService(serviceType);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Logout();
        }
    }
}
