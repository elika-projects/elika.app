﻿using System.Security.Principal;

namespace Elika.App.Auth
{
    /// <summary>
    ///     Хранилище основной информации клиента
    /// </summary>
    public interface IPrincipalStore
    {
        /// <summary>
        ///     Установить основную информацию клиента
        /// </summary>
        /// <param name="principal">Основная информация</param>
        void SetPrincipal(IPrincipal principal);

        /// <summary>
        ///     Получить основную информацию
        /// </summary>
        /// <returns></returns>
        IPrincipal GetPrincipal();
    }
}
