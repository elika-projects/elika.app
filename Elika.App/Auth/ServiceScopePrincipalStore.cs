﻿using System.Security.Principal;

namespace Elika.App.Auth
{
    /// <summary>
    ///     Хранилище основной информации пользователя в текущем пространсве сервисов
    /// </summary>
    public class ServiceScopePrincipalStore : IPrincipalStore
    {
        private IPrincipal _principal;

        /// <inheritdoc />
        public void SetPrincipal(IPrincipal principal)
        {
            _principal = principal;
        }

        /// <inheritdoc />
        public IPrincipal GetPrincipal()
        {
            return _principal ?? ElikaPrincipal.Empty;
        }
    }
}
