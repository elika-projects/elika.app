﻿using System;
using System.Security.Principal;
using System.Threading;

namespace Elika.App.Auth
{
    /// <summary>
    ///     Хранилище основной информации пользователя в текущем потоке
    /// </summary>
    public class ThreadPrincipalStore : IPrincipalStore
    {
        /// <inheritdoc />
        public void SetPrincipal(IPrincipal principal)
        {
            AppDomain.CurrentDomain.SetThreadPrincipal(principal);
        }

        /// <inheritdoc />
        public IPrincipal GetPrincipal()
        {
            return Thread.CurrentPrincipal;
        }
    }
}
