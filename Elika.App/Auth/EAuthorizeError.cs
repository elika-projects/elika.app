﻿namespace Elika.App.Auth
{
    /// <summary>
    ///     Результаты авторизации
    /// </summary>
    public enum EAuthorizeError
    {
        /// <summary>
        ///     ОК
        /// </summary>
        OK,

        /// <summary>
        ///     Пользователь не найден
        /// </summary>
        UserNotFound,

        /// <summary>
        ///     Пароль не корректен
        /// </summary>
        PasswordIncorrect,

        /// <summary>
        ///     Пользователь заблокирован
        /// </summary>
        UserBlocked,

        /// <summary>
        ///     Неопределённая ошибка
        /// </summary>
        Error
    }
}
