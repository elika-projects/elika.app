﻿using System;

namespace Elika.App.Auth
{
    /// <summary>
    ///     Информация о сессии
    /// </summary>
    public class SessionInfo : ICloneable
    {
        /// <summary>
        ///     Токен доступа
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        ///     Тип аунтетификации
        /// </summary>
        public string AuthenticationType { get; set; }

        /// <inheritdoc />
        public object Clone()
        {
            return new SessionInfo()
            {
                AuthenticationType = AuthenticationType,
                AccessToken = AccessToken
            };
        }
    }
}
