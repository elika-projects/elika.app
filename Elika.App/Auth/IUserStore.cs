﻿namespace Elika.App.Auth
{
    /// <summary>
    ///     Интерфейс хранилища пользователя
    /// </summary>
    public interface IUserStore
    {
        /// <summary>
        ///     Авторизация пользователя
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="password">Пароль</param>
        /// <returns>Информация о пользователе</returns>
        OperationResult<SessionInfo, EAuthorizeError> Authorize(string userName, string password);

        /// <summary>
        ///     Получить информации о текущем пользователе
        /// </summary>
        /// <param name="sessionKey">Ключ сессии</param>
        /// <returns>Информация о пользователе</returns>
        OperationResult<UserInfo> GetCurrentUser(string sessionKey);
    }
}
