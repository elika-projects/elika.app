﻿namespace Elika.App
{
    internal class UserScopeKeys
    {
        public static string UserRoles { get; } = "USER_ROLES";

        public static string UserSession { get; } = "USER_SESSION";

        public static string UserInfo { get; } = "USER_INFO";
    }
}
