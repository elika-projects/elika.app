﻿using System;
using System.Linq;
using Elika.App.Auth;
using Elika.App.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace Elika.App
{
    /// <summary>
    ///     Расширения для <see cref="IServiceCollection"/>
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        ///     Сборка зависимостей в <see cref="ElikaApplication"/>
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Собранное приложение Elika</returns>
        public static ElikaApplication BuildElikaApplication(this IServiceCollection services)
        {
            services.AddScoped<UserManager>();

            if (!services.Any(descriptor => descriptor.ServiceType.IsParentOf(typeof(IPrincipalStore))))
                services.AddSingleton<IPrincipalStore, ServiceScopePrincipalStore>();

            var app = new ElikaApplication(services);
            services.AddSingleton(app);

            services.AddSingleton(p => new UserScope(p))
                .AddTransient(p => p.GetRequiredService<UserScope>().Principal);

            return app;
        }
    }
}
