﻿using System;
using Elika.App.Auth;

namespace Elika.App.Managers
{
    internal class UserManager
    {
        private readonly IUserStore _userStore;
        private readonly IPrincipalStore _principalStore;
        private readonly UserScope _userScope;

        public UserManager(IUserStore userStore, IPrincipalStore principalStore, UserScope userScope)
        {
            _userStore = userStore;
            _principalStore = principalStore;
            _userScope = userScope;
        }

        public void Authorize(string userName, string password)
        {
            var result = _userStore.Authorize(userName, password);

            if (!result.IsSuccess)
            {

            }

            var sessionInfo = result.Result;

            if (sessionInfo == null)
                throw new InvalidOperationException();

            _userScope.SetSessionInfo(sessionInfo);
            UpdateCurrentUserInfo();

            var principal = new ElikaPrincipal(_userScope);
            _principalStore.SetPrincipal(principal);
        }

        public void UpdateCurrentUserInfo()
        {
            var sessionInfo = _userScope.GetSessionInfo();
            _userStore.GetCurrentUser(sessionInfo.AccessToken);
        }

        public void Logout()
        {
            _principalStore.SetPrincipal(null);
        }
    }
}
