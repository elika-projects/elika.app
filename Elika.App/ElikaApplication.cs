﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Elika.Collection;
using Microsoft.Extensions.DependencyInjection;

namespace Elika.App
{
    /// <summary>
    ///     Приложение Elika
    /// </summary>
    public class ElikaApplication : DataCollection
    {
        private readonly object _locker = new object();

        private readonly IServiceCollection _services;

        private List<UserScope> _userScopes = new List<UserScope>();

        internal ElikaApplication(IServiceCollection services)
        {
            _services = services;
        }

        /// <summary>
        ///     Создать новое пространство пользователя
        /// </summary>
        /// <returns>Пространство пользователя</returns>
        public UserScope AddUserScope()
        {
            var scope = _services.BuildServiceProvider().GetRequiredService<UserScope>();
            _userScopes.Add(scope);
            return scope;
        }

        /// <summary>
        ///     Создать новое пространство пользователя
        /// </summary>
        /// <returns>Пространство пользователя</returns>
        public void RemoveUserScope(UserScope userScope)
        {
            lock (_locker)
            {
                if (!_userScopes.Contains(userScope))
                    return;

                _userScopes.Remove(userScope);
                userScope.Dispose();
            }
        }
    }
}
